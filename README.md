# Curso Docker

Esse projeto é para armazenar o curso docker: Ferramenta essencial da plataforma Udemy


# Repositorio do git do curso
https://github.com/cod3rcursos/curso-docker

# Comandos docker

docker container run <nome da Imagem> -> Cria e executa o container com a imagem;(Toda vez que é executado esse comando ele cria um container novo, caso não informe um nome o docker cria um nome randomizado)

docker container run --rm <nome da Imagem> -> Cria e executa o container com a imagem , após terminar a execução ele remove o container;

docker container ps || docker container ls -> mostrar todos os container que estão em executados;

docker container ps -a || docker container ls -a -> mostrar todos os container que foram criados;

docker container run --name <nome do Container> <nome da Imagem> -> Cria e executa o container com o nome que foi escrito da imagem;(O nome é unico, caso tente usar um nome que já existe ele da erro)

docker container start <nome do Container> -> Executa um container já criado

docker container run --name <nome do Container> -p <Porta que está ser exposta para fora do container, ou seja, a porta que vai acessar o container: porta interna que vai acesar a imagem > <nome da Imagem> -> Cria e executa o container com o nome que foi escrito da imagem usando as portas especificadas;

docker container stop <nome do Container> -> Pare o container que está em execução com o nome especificado;

docker container restart <nome do Container> -> Renicia o container que está em execução com o nome especificado;

docker container rm <nome do Container> -> Remove o container com o nome especificado

docker container logs <nome do Container> -> Mostar o logs do container;

docker container exec <nome do Container> <comando> -> executa um comando dentro do container;

docker image ls -> Mosta todas as imagens baixadas;

docker volume ls -> Mosta todos os volumes criados;
docker image rm <nome da Imagem> || docker image rm <imagem ID> -> remove a imagem;

docker image build -t <nome da imagem que você quer> <local onde está o dockerfile> -> Cria a imagem com o nome que você quer e haver com o dockerfile que foi especificado


docker compose up
# Cronstruindo uma imagem

1 - Crie um dockerfile(o nome do arquivo tem que ser assim);


